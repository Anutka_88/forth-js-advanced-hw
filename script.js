const listContainer = document.querySelector(".list");

function renderName(data) {
  const list = data.map(({ episodeId, name, openingCrawl }) => {
    return `<li class='movie-name'><b>Episode</b>: ${episodeId} <br><b>Name</b>: ${name} <br><b>Description</b>: ${openingCrawl}</li>`;
  });
  listContainer.insertAdjacentHTML("afterbegin", list.join(""));
}

function renderCharacters(data, i) {
  const movieItem = document.querySelectorAll(".movie-name");
  const ul = document.createElement("ul");
  const p = document.createElement("p");
      p.innerHTML = "<b>Characters:</b>";
    const listChar = data.map(({ name }) => `<li>${name}</li>`);
    ul.insertAdjacentHTML("afterbegin", listChar.join(""));
    movieItem[i].append(p);
    movieItem[i].append(ul);

}

fetch ('https://ajax.test-danit.com/api/swapi/films')
  .then((resp) => resp.json())
  .then((data) => {
    renderName(data);

    data.forEach((item, index) => {
      const promises = item.characters.map((i) =>
        fetch(i)
        .then((resp) => resp.json())
      );
      Promise.all(promises).then((data) => {
        renderCharacters(data, index);
      });
    });
  })
  .catch((err) => {
    console.log("There is an error:", err);
  });

    
    
    
    